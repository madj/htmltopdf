package htmltopdf

import (
	"fmt"
	"os/exec"
	"regexp"
	"strings"
)

func CreatePdf(input string, options []string) (data []byte, err error) {

	regExp := `^((https?:\/\/)(www\.)|(https?:\/\/)|(www\.))([^<\/\?\s]+)[^<\s]`
	isUrl, err := regexp.MatchString(regExp, input)
	if err != nil {
		fmt.Println("Error Matching RexExp: ", err)
	}

	if isUrl {
		//fmt.Println("url")
		options = append(options, input)
		options = append(options, "-")
		//fmt.Println(options)
		cmd := exec.Command("wkhtmltopdf", options...)
		data, err = cmd.Output()
	} else {
		options = append(options, "-")
		options = append(options, "-")
		//fmt.Println(options)
		dataString := strings.NewReader(input)
		cmd := exec.Command("wkhtmltopdf", options...)
		cmd.Stdin = dataString
		data, err = cmd.Output()
	}

	return data, err
}
